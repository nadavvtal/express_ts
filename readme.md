# Welcome to express_ts 👋
[![ts](https://badgen.net/badge/-/node?icon=typescript&label&labelColor=blue&color=555555)](https://github.com/TypeStrong/ts-node)
 ![ts](https://badgen.net/badge/-/TypeScript/blue?icon=typescript&label)
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)

> express ts intro project

### 🏠 [Homepage](https://bitbucket.org/nadavvtal/express_ts/src)
## Author

👤 **Nadav Tal**

* Website: https://github.com/nadavadan
* Github: [@nadavadan](https://github.com/nadavadan)
* LinkedIn: [@Nadav tal](https://linkedin.com/in/Nadav tal)

## Show your support
Give a ⭐️ if this project helped you!


