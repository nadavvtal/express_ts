import express from 'express';

const router = express.Router();
var contactMiddle = function (req:express.Request, res:express.Response, next:express.NextFunction):void {
  console.log('Phones and mails should be available, user want to contact');
  next()  
}
var helpMiddle = function (req:express.Request, res:express.Response, next:express.NextFunction):void {
  console.log(`Help section is now in use`);
  next()  
}
router.use(helpMiddle);
router.use(express.json())

router.get('/', function (req:express.Request, res:express.Response) {
    res.send('this is help page, we have got lot of ways to help you');
  })
  
router.get('/contact',contactMiddle, function (req:express.Request, res:express.Response) {
    res.send(`you can call us - 01-2345678`);
})

router.post('/openCase', function (req:express.Request, res:express.Response) {
  let bodyLength = Object.keys(req.body).length
  res.send(`Your have ${bodyLength} technical issues to solve.`);
})

router.use("*",(req:express.Request,res:express.Response)=>{
    res.status(404).send("You realy need help..")
})
  
export default router;




