import express from 'express';

const router = express.Router();
var userMiddle = function (req:express.Request, res:express.Response, next:express.NextFunction):void {
    console.log(`This is each user middle function `);
    next()  
  } 

router.get('/', function (req:express.Request, res:express.Response) {
    res.send('users page');
  })
  
router.get('/:id',userMiddle, function (req:express.Request, res:express.Response) {
    res.send(`this is user : ${req.params.id} middle functiomn`);
})

var usersMiddle = function (req:express.Request, res:express.Response, next:express.NextFunction):void {
    console.log(`This is users routing group`);
    next()  
  }

router.use(usersMiddle)
router.use("*",(req,res)=>{
    res.status(404).send("Are you sure you know our users?");
})
  
export default router;



