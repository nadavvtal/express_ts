import express from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRoute from "./users.js"
import helpRoute from "./help.js"

const { PORT, HOST } = process.env;
const app = express()
app.use('/users',usersRoute);
app.use("/help",helpRoute);
app.use( morgan('dev') );
app.use(express.json())

var myMiddle = function (req :express.Request, res:express.Response, next:express.NextFunction):void {
    console.log("My logger :)")
    console.log(`be ready for ${Object.keys(req.query).length} query params`)
    next()      
  } 
app.use(myMiddle);

app.get('/', (req, res) => {
    res.status(200).send('Hello Express!')
})

app.post('/blog/blogpost', (req:express.Request, res:express.Response) => {
    const body = req.body;
    res.status(200).send(body);
});
    
app.get('/cookies/:myCookie/:myFlavors', (req:express.Request, res:express.Response) => {
    res.set("Content-Type", "text/html");
    res.status(200).send(`<h1>my favorite cookie is ${req.params.myCookie}, best if its ${req.params.myFlavor}<h1>`);
});

app.use((req:express.Request,res:express.Response)=>{
    res.status(404).send("Try another path bro..")
})
app.listen(Number(PORT),String(HOST), () => {
    log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
});
